const stripe = require('stripe')(process.env.SecretKey)
//Sample data
// {
// 	"amount" : 999,
//  "currency" : "eur"
// 	"source" : "tok_mastercard",
// 	"receipt_email" : "someone@example.com",
// 	"metadata": {"order_id": "6735"}
// }

async function postCharge(req, res) {
    try {
        const charge = await stripe.charges.create({
            amount: req.body.amount,
            currency: req.body.currency,
            source: req.body.source, // obtained with Stripe.js
            receipt_email: req.body.receipt_email,
            metadata: req.body.metadata
        })

        if (!charge) throw new Error('charge unsuccessful')

        res.status(200).json({
            message: 'charge posted successfully',
            charge
        })
    } catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

module.exports = postCharge