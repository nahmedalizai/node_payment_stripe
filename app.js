const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const postCharge = require('./stripe')
require('dotenv').config()

const app = express()
const router = express.Router()

router.post('/stripe/charge', postCharge)

router.all('*', (_, res) =>
    res.json({ message: 'please make a POST request to /stripe/charge' })
)

app.use((_, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    )
    next()
})

app.use(bodyParser.json())

app.use('/api', router)

app.use(express.static(path.join(__dirname, '../build')))

app.get('/', (req, res) => res.send("Stripe payment api"))

module.exports = app